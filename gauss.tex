%format latexg
%
\documentclass[12pt]{article}
\usepackage{circuitikz}
\topmargin 0pt \headheight 12pt \headsep 6pt
\topskip 10pt
\parindent0pt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\input{definitions.tex}

\def\normalcoord(#1){coordinate(#1)}
\def\showcoord(#1){node[circle, red, draw, inner sep=1pt,
    pin={[blue, overlay, inner sep=0.5pt, font=\tiny, pin distance=0.1cm,
    pin edge={green, overlay}]45:#1}](#1){}}
\let\coord=\normalcoord
%\let\coord=\showcoord
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{document}
%
\centerline{\Large How to crunch Gauss integrals}
\bigskip

Consider the integral
\begin{equation}
I_n(\sig) = \Intall x^n \Gexp \ds{x} \,.
\end{equation}

For odd $n$ the integrand $x^n \Gexp$ is an odd function. 
Therefore
\begin{equation}
I_n(\sig) = 0 \qquad\mbox{if $n$ odd}
\end{equation}
due to the symmetry of the integral.
\medskip

For even $n$ we derive $I_n(\sig)$ by $\sig$ and obtain the recurrence relation
\begin{eqnarray}
\dod{\sig}\,I_n(\sig) 
&=& \dod{\sig}\Intall x^n\, \Gexp \ds{x}            \nnnl
&=& \Intall x^n \,\dod{\sig}\, \Gexp \ds{x}         \nnnl
&=& \Intall x^n \,{x^2\over\sig^3}\, \Gexp \ds{x}   \nnnl
&=& {1\over\sig^3}\Intall x^{n+2}\, \Gexp \ds{x}    \nnnl
&=& {1\over\sig^3}\, I_{n+2}(\sig) \,.
\end{eqnarray}

Therefore, the only integration we have to carry out is
\begin{equation}
I_0(\sig) = \Intall\Gexp \ds{x} \,.
\end{equation}

In order to evaluate this integral, we integrate the {\em square\/} of $I_0$ 
and introduce polar coordinates:
\begin{eqnarray}
I_0^2(\sig) 
&=& \Intall\Gexp \ds{x} \cdot \Intall\gexp{y}{\sig} \ds{y}      \nnnl
&=& \Intall\Intall e^\supf{-}{x^2+y^2}{2\sig^2} \ds{x}\ds{y}    \nnnl
&=& \Int_0^\tp\!\d{\vph}\Int_0^\infty\!\d{r}\, r\,\gexp{r}{\sig}\nnnl
&=& \tp \Int_0^\infty\!\d{r}\, r\,\gexp{r}{\sig}                \nnnl
&=& \pi \Int_0^\infty\!\d{(r^2)}\, \gexp{r}{\sig}               \nnnl
&=& -\tp\sig^2 \, \gexp{r}{\sig}\Range_0^\infty                 \nnnl
&=& \tp\sig^2 \,.
\end{eqnarray}

Conse{\em qu\/}ently, we obtain
\begin{equation}
I_0(\sig) = \sqrt{\tp}\sig
\end{equation}
yielding the recurrence relation for $I_{2n}(\sig)$
\begin{eqnarray}
I_{2n}(\sig)
&=& \left[ \sig^3\,\dod{\sig} \right]^n \, I_0(\sig)        \nnnl
&=& \sqrt{\tp} \prod_{k=1}^n (2k-1) \cdot \sig^{2n+1} \,.
\end{eqnarray}

\bigskip
{\large Generalized Gauss Integrals}
\medskip

There is a generalization to this solution to solve integrals of the type

\begin{equation}
\Gamma(a,b,c) := \Intall\!\d{x}\,e^{-ax^2+bx+c} \,.
\end{equation}

First, we express the exponent as
\begin{equation}
-ax^2+bx+c = -a\left(x-{b\over2a}\right)^2+{b^2\over4a}+c \,,
\end{equation}
assuming $a$ to be positive real, $b$ and $c$ to be real.

We then get
\begin{eqnarray}
\Gamma(a,b,c) 
&=& e^{{b^2\over4a}+c}\Intall\!\d{x}\,e^{-a\left(x-{b\over2a}\right)^2} \nnnl
&=& e^{{b^2\over4a}+c}\Intall\!\d{\xi}\,e^{-a\xi^2}                     \nnnl
&=& \sqrt{\pi\over a}\,e^{{b^2\over4a}+c} \,.                   \label{GenGauss}
\end{eqnarray}

This formula can immediately be generalized to complex $c$ as the integral after
substituting $\xi$ for $x$ is real.
\medskip

It can also be proven that (\ref{GenGauss}) remains valid for complex $b$ and for
complex $a$ with positive Re($a$).

For purely imaginary $a$ (\ref{GenGauss}) is only valid if $b$ is also purely 
imaginary. Integrals of this type are called {\em Fresnel\/} integrals.

\pagebreak
\centerline{\Large How to draw fancy circuit diagrams}
\bigskip
{\large {\bf circuitikz\/} is an incredibly fancy schematic package in \LaTeX}
\medskip

Here's a first shot, copied and pasted from the manual:
\begin{center}
\begin{circuitikz} \draw
(0,0) node[pnp, color=blue] (pnp2) {}
(pnp2.B) node[pnp, xscale=-1, anchor=B, color=brown] (pnp1) {}
(pnp1.C) node[npn, anchor=C, color=green] (npn1) {}
(pnp2.C) node[npn, xscale=-1, anchor=C, color=magenta] (npn2) {}
(pnp1.E) -- (pnp2.E) (npn1.E) -- (npn2.E)
(pnp1.B) node[circ] {} |- (pnp2.C) node[circ] {}
;\end{circuitikz}
\begin{circuitikz} \draw
(0,0) node[pnp, color=blue] (pnp2) {Qp2}
(pnp2.B) node[pnp, xscale=-1, anchor=B, color=brown] (pnp1) {Qp1}
(pnp1.C) node[npn, anchor=C, color=green] (npn1) {Qn1}
(pnp2.C) node[npn, xscale=-1, anchor=C, color=magenta] (npn2) {Qn2}
(pnp1.E) -- (pnp2.E) (npn1.E) -- (npn2.E)
(pnp1.B) node[circ] {} |- (pnp2.C) node[circ] {}
;\end{circuitikz}
\end{center}

Let's try out a few things.

\begin{circuitikz}[american]
\draw (0,0) to[isource, l=$I_0$] (0,3) --
(2,3)
to[R=$R_1$] (2,0) -- (0,0);
\draw (2,3) -- (4,3) to[R=$R_2$]
(4,0) -- (2,0);
\end{circuitikz}
\begin{circuitikz}
\draw (0,0) to[isource, l=$I_0$] (0,3) --
(2,3)
to[R=$R_1$] (2,0) -- (0,0);
\draw (2,3) -- (4,3) to[R=$R_2$]
(4,0) -- (2,0);
\end{circuitikz}

\begin{circuitikz}[american]
\draw (0,0) to[isource, l=$I_0$] (0,3)
to[short, -*, i=$I_0$] (2,3)
to[R=$R_1$, i=$i_1$] (2,0) -- (0,0);
\draw (2,3) -- (4,3)
to[R=$R_2$, i=$i_2$]
(4,0) to[short, -*] (2,0);
\end{circuitikz}
\begin{circuitikz}
\draw (0,0) to[isource, l=$I_0$] (0,3)
to[short, -*, i=$I_0$] (2,3)
to[R=$R_1$, i<=$i_1$] (2,0) -- (0,0);
\draw (2,3) -- (4,3)
to[R=$R_2$, i<=$i_2$]
(4,0) to[short, -*] (2,0);
\end{circuitikz}

\begin{circuitikz}
\draw (0,0) to[isource, l=$I_0$] (0,3)
to[short, -*, i=$I_0$] (2,3)
to[R=$R_1$, i<^=$i_1$] (2,0) -- (0,0);
\draw (2,3) -- (4,3)
to[R=$R_2$, i<^=$i_2$]
(4,0) to[short, -*] (2,0);
\end{circuitikz}
\begin{circuitikz}[american]
\draw (0,0) to[isource, l=$I_0$] (0,3)
to[short, -*, i=$I_0$] (2,3)
to[R=$R_1$, i<=$i_1$] (2,0) -- (0,0);
\draw (2,3) -- (4,3)
to[R=$R_2$, i>_=$i_2$]
(4,0) to[short, -*] (2,0);
\end{circuitikz}

\begin{circuitikz}[european] %, voltage shift=0.5]
\draw (0,0) to[isource, l=$I_0$, v=$V_0$]
(0,3)
to[short, -*, i=$I_0$] (2,3)
to[R=$R_1$, i<_=$i_1$] (2,0) -- (0,0);
\draw (2,3) -- (4,3)
to[R=$R_2$, i<_=$i_2$]
(4,0) to[short, -*] (2,0);
\end{circuitikz}

This is {\it very\/} confusing.

Let's have some Romano cheese.

\begin{circuitikz}[european]
\draw (0,0) node[npn](Q){};
\path (Q.center) (Q.B) (Q.C) (Q.E);
\end{circuitikz}
\begin{circuitikz}[american,]
\draw (0,0) node[npn](Q){};
\path (Q.center) \coord(center) (Q.B) \coord(B) (Q.C) \coord(C) (Q.E) \coord(E);
\end{circuitikz}

\begin{circuitikz}[european]
\draw (0,0) node[nfet](MN){};
\draw (0,1) node[pfet](MP){};
\end{circuitikz}
\begin{circuitikz}[american]
\draw (0,-1) node[nfet](MN){};
\draw (0,1) node[pfet](MP){};
\end{circuitikz}

Ok, now let's do something useful

\begin{circuitikz}[european]
\draw (0,0) node[nfet](MDUT){MDUT}
(MDUT.S) to[short, -*] (0,-1.5) -- (-2.5,-1.5) 
to[vsource, l=$V_{d,ref}$] (-2.5,1.5) to[short, -o] (-.5,1.5);
\draw (0,-1.5) node[rground, anchor=center](SGND){};
\draw (1,-1.5) to[short, *-] (2.5,-1.5) -- (2.5,4) -- (0,4) 
to[isource, l=$I_d$] (0,2.5) to[cvsource, l=$\mbox{gain:}\,A$] (0,1) -- (MDUT.D);
\draw (0,2.5) to[short, *-] (-1.5,2.5) -- (-1.5,0) -- (MDUT.G);
\draw (0,1) to[short,*-] (-1,1) -- (-1,2) to[short, -o] (-.5,2);
\draw (MDUT.B) -- (1,0) to[vsource, l=$V_{b}$] (1,-1.5) -- (0,-1.5);
\draw (0,1) node[right] {$V_d$} (0,2.5) node[right] {$V_g$};
\end{circuitikz}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\end{document}
