\documentclass[journal]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage{circuitikz}
\usepackage{amsmath}
\usepackage{color}
\hyphenation{op-tical net-works semi-conduc-tor IEEEtran}
\def\le{\left}
\def\ri{\right}
\def\nnnl{\nonumber\\}
\def\um{\,\mu\mathrm{m}}
\def\comment#1{{\sf #1\/}} 
% paper title
\begin{document}
\title{Eine Simulationsschaltung zur Charakterisierung von Transistoren}
\author{Christoph Maier~\IEEEmembership{IEEE-Mitglied}% <-this % stops a space
\thanks{Copyright \copyright 2014--2020, Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license.}}%
%~\ref{http://creativecommons.org/licenses/by-nc-sa/4.0/}}% <-this % stops a space
\maketitle
\begin{abstract}\boldmath
Dieser Artikel stellt eine einfache Simulationsschaltung
zur Extraktion von Transistor-Kleinsignalparametern vor,
die für die analoge Schaltungsentwicklung wichtig sind:
Transkonduktanz~$g_m$, Transkonduktanz per Drainstrom~$g_m/I_d$
und Kleinsignal-Spannungsverstärkung~$g_m/g_o$.
\end{abstract}


%\begin{IEEEkeywords}
%\ldots
%\end{IEEEkeywords}

\IEEEpeerreviewmaketitle
%%
\section{Die Schaltung}
%%
\begin{figure}[h]
\centering
\begin{circuitikz}[european]
\draw (0,0) node[nfet](MDUT){MDUT}
(MDUT.S) to[short, -*] (0,-1.5) -- (-2.5,-1.5) 
to[vsource, l=$V_{d,ref}$] (-2.5,1.5) to[short, -o] (-.5,1.5);
\draw (0,-1.5) node[rground, anchor=center](SGND){};
\draw (1,-1.5) to[short, *-] (2.5,-1.5) -- (2.5,4) -- (0,4) 
to[isource, l=$I_d$] (0,2.5) to[cvsource, l=$\mbox{Verstärkung:}\,A$] (0,1) -- (MDUT.D);
\draw (0,2.5) to[short, *-] (-1.5,2.5) -- (-1.5,0) -- (MDUT.G);
\draw (0,1) to[short,*-] (-1,1) -- (-1,2) to[short, -o] (-.5,2);
\draw (MDUT.B) -- (1,0) to[vsource, l=$V_{b}$] (1,-1.5) -- (0,-1.5);
\draw (0,1) node[right] {$V_d$} (0,2.5) node[right] {$V_g$};
\end{circuitikz}
\caption{Simulationsschaltung zur Charakterisierung von MOSFETs}
\label{fig:schematics}
\end{figure}
%
Die wesentlichen Entwurfsparameter für die Dimensionierung von Mosfets
sind deren Drainstrom~$I_d$, der die Transkonduktanz bestimmt, 
und die Drain-Source-Spannung~$V_{ds}$, 
die die Kleinsignal-Ausgansleitfähigkeit~$g_o$ bestimmt.
Der Arbeitspunkt des Transistors ist aber hauptsächlich 
durch die Gate-Source-Spannung~$V_{gs}$ bestimmt.

Dieses Problem wird hier durch eine Rückkopplungsschleife 
mit einer idealen spannungsgesteuerten Spannungsquelle (VCVS)
mit hoher Spannungsverstärkung $A>10^3$ gelöst,
die die Gatespannung~$V_g$ so regelt, 
dass für einen gegebenen Drainstrom~$I_d$ 
die Drainspannung~$V_d$ gleich einer Referenzspannung~$V_{d,ref}$ ist. 
Abbildung~\ref{fig:schematics} zeigt das Schaltbild für einen NMOS-Transistor.

Kirchhoffs Knotenregel ergibt das Gleichungssystem
\begin{IEEEeqnarray}{c}
I_d = g_m V_g + g_o V_d \label{eqn:KCL}\\
V_g = V_d + A \le(V_d - V_{d,ref}\ri) \nonumber
\end{IEEEeqnarray}
das für die Gatespannung zum Ausdruck
\begin{equation}\label{eqn:Vg}
V_g = \frac{I_d-g_o\,V_{d,ref}\,A/\le(1+A\ri)}{g_m + g_o/\le(1+A\ri)}
\end{equation} 
und für die Drainspannung zu
\begin{equation}\label{eqn:Vd}
V_d = \frac{I_d+A\,g_m\,V_{d,ref}}{\le(1+A\ri) g_m + go}
\end{equation} 
führt.

Im Idealfall unendlicher Spannungsverstärkung $A\rightarrow\infty$
wird die Gatespannung zu 
\begin{equation}\label{eqn:Vg_ideal}
V_g = \frac{I_d-g_o\,V_{d,ref}}{g_m}
\end{equation} 
und die Drainspannung zu
\begin{equation}\label{eqn:Vd_ideal}
V_d = V_{d,ref}\,.
\end{equation} 

\section{Parameterextraktion}
%%
\subsection{Transkonduktanz}
%
Die Transkonduktanz $g_m$ in Abhängigkeit des Drainstroms
erhält man, indem man~$I_d$ für festes~$V_d$ variiert, 
aus dem Ausdruck 
\begin{equation}\label{eqn:gm}
1/\le(\frac{\partial V_g}{\partial I_d}\ri) = g_m+g_o/\le(1+A\ri) 
\overset{A\rightarrow\infty}{\approx} g_m\,.
\end{equation}

\subsection{$g_m/I_d$}
%
The spezifische Transkonduktanz~$g_m/I_d$ ist ein nützlicher Entwurfsparameter, 
um den Arbeitspunkt eines Transistors einzustellen.
$g_m/I_d$ ist im Subthreshold-Arbeitsbereich eines Transistors maximal.
Man findet diesen Parameter, wenn man $I_d$ für konstantes $V_d$ variiert, 
aus dem Ausdruck
\begin{equation}\label{eqn:gm_over_Id}
1/\le(\frac{\partial V_g}{\partial I_d}\,I_d\ri) = \frac{g_m+g_o/\le(1+A\ri)}{I_d}
\overset{A\rightarrow\infty}{\approx} \frac{g_m}{I_d}\,.
\end{equation}

\subsection{$g_m/g_o$}
%
Der nach $g_m$ oder $g_m/I_d$ wichtigste Entwurfsparameter 
zur Dimensionierung eines Transistors 
ist die Kleinsignal-Ausgangsleitfähigkeit~$g_o$. 
Mit der Schaltung in Abbildung~\ref{fig:schematics}, 
kann die intrinsische Spannungsverstärkung~$g_m/g_o$ 
durch Variation von~$V_{d,ref}$ bei konstantem~$I_d$ bestimmt werden:
\begin{equation}\label{egn:gm_over_go}
-\le(\frac{\partial V_d}{\partial V_{d,ref}}\ri)
/\le(\frac{\partial V_g}{\partial V_{d,ref}}\ri)
= \frac{\frac{A\,g_m}{\le(1+A\ri)g_m+g_o}}{\frac{A\,g_o}{\le(1+A\ri)g_m+g_o}} 
= \frac{g_m}{g_o} \,.
\end{equation}
\end{document}


