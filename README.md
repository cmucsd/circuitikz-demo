# circuitikz-demo #

An article about small signal mosfet characterization as a demo for LaTeX [CircuitIKZ](https://ctan.org/pkg/circuitikz?lang=en)
based on my old [transistorcharacterization](https://bitbucket.org/cmucsd/transistorcharacterization) repository from 2014.

Ein Artikel zur Kleinsignalcharakterisierung von Mosfets als Beispiel 
für das LaTeX-[CircuitIKZ](https://ctan.org/pkg/circuitikz?lang=de)-Paket,
basierend auf meinem alten [transistorcharacterization](https://bitbucket.org/cmucsd/transistorcharacterization) repository von 2014.

### Compiled under ###
Ubuntu Mate 16.04 LTS

pdfTeX, Version 3.14159265-2.6-1.40.16 (TeX Live 2015/Debian) (preloaded format=pdflatex 2018.10.12)

Das deutsche .tex-File ist unter emacs als UTF8-Unix gespeichert worden. 

### Copyright ###
Copyright (c) 2014--2020 under 
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
by Christoph Maier